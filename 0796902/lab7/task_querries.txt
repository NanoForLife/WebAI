1. How many airlines inside?
MATCH (n)-[:type]->(:Airline) RETURN count(n);
2. How many aircrafts inside? how many are active? Inactive?
MATCH (n)-[:type]->(:Aircraft) return count(n) 
MATCH (n)-[:type]->(:Aircraft) where n.hasStatus = 'Active' return count(n)
MATCH (n)-[:type]->(:Aircraft) where n.hasStatus = 'Inactive' return count(n)
3. Return the number of active aircrafts from each airline.
MATCH (n)-[:Owns]->(a) 
WHERE a.hasStatus = "Active"
RETURN n,count(a);
4. How many aircrafts were created before 2010?
MATCH (n)-[:type]->(:Aircraft) 
where n.hasCreationDate < "2010"
return count(n)
5. How many aircrafts were modified? how many were modified in the second year of its
creation?
MATCH (n)-[:type]->(:Aircraft)
where n.hasModificationDate is not null
RETURN count(n);
MATCH (n)-[:type]->(:Aircraft)
WITH [item in split(n.hasCreationDate, "_") | toInteger(item)] AS dateComponents
, [item in split(n.hasModificationDate, "_") | toInteger(item)] AS mod
where dateComponents[0]+1 = mod[0]
RETURN count(mod);
6. How many aircraft model existed in total?
Match (n)-[:hasAircraftModel]->(p)
return count(distinct p)
7. Return all the aircraft models that have more than 5 versions.
MATCH (n)-[:hasModelVersion]->(p)
WITH n, count(p) AS pl
where pl > 5
RETURN n,pl;
8. Return all the companies that manufacture these aircrafts.
MATCH (n)-[:hasModelVersion]->(p)
WITH n, count(p) AS pl
where pl > 5
RETURN n.isManufacturedBy;
9. How many aircraft models lack labels indicating their manufacturing companies?
MATCH (n)-[:type]->(:AircraftModel)
where n.isManufacturedBy is null
RETURN count(n);
10. Get all the properties for all the instances of all classes. (Property keys)
MATCH (n)-[:type]->(p)
WITH KEYS(n) AS keys,n
UNWIND keys AS key
RETURN DISTINCT n, key AS props;