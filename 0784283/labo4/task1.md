1.
SELECT ?s
WHERE{
 ?s a rdfs:Class.
}

2.
prefix ex: <http://GP-onto.fi.upm.es/exercise2#>

SELECT ?s
WHERE{
 ?s rdfs:subClassOf ex:Establishment.
}

3.
prefix ex: <http://GP-onto.fi.upm.es/exercise2#>

SELECT ?s
WHERE{
 ?s a ex:City.
}

4.
prefix ex: <http://GP-onto.fi.upm.es/exercise2#>

SELECT ?o
WHERE{
 ex:Santiago_de_Compostela ex:hasInhabitantNumber ?o.
}

5.
prefix ex: <http://GP-onto.fi.upm.es/exercise2#>

SELECT ?o
WHERE{
 VALUES ?places{ex:Santiago_de_Compostela ex:Arzua}
 ?places ex:hasInhabitantNumber ?o.
}

6.
prefix ex: <http://GP-onto.fi.upm.es/exercise2#>

SELECT ?s ?o
WHERE{
 ?s ex:hasInhabitantNumber ?o.
}
ORDER BY ?s

7.
prefix ex: <http://GP-onto.fi.upm.es/exercise2#>

SELECT ?o
WHERE{
    ?types rdfs:subClassOf ex:Locality .
    ?s a ?types.
    FILTER EXISTES {?s ex:hasInhabitantNumber ?o.}
    ?s ex:hasInhabitantNumber ?o.
}

8.
prefix ex: <http://GP-onto.fi.upm.es/exercise2#>

SELECT ?s ?o
WHERE {
  ?s ex:hasInhabitantNumber ?o.
  FILTER (xsd:integer(?o) > 200000)
}

9:
prefix ex: <http://GP-onto.fi.upm.es/exercise2#>
SELECT ?s ?n
WHERE {
    ex:Pazo_Breogan ex:hasAddress ?address.
    ?address ex:hasStreet ?s.
    ?address ex:hasNumber ?n.
}
 
10:
prefix ex: <http://GP-onto.fi.upm.es/exercise2#>
SELECT DISTINCT ?s
WHERE {
    ?s rdfs:subClassOf ex:Location
}

11:
prefix ex: <http://GP-onto.fi.upm.es/exercise2#>
SELECT DISTINCT ?p 
WHERE {
    ?class rdfs:subClassOf ex:Locality.
    ?p a ?class.
}

12:
describe ?resource
WHERE {
 ?resource rdfs:label "Madrid"
}

13:
prefix ex: <http://GP-onto.fi.upm.es/exercise2#>
construct {
    ?place ex:isIn ?province
}
WHERE {
    ?s rdfs:subClassOf ex:TouristicLocation.
    ?place a ?s.
    ?place ex:isPlacedIn ?city.
    ?city ex:inProvince ?province.
}

14:
prefix ex: <http://GP-onto.fi.upm.es/exercise2#>
ASK{ 
    ?s ?p ex:Town 
}


