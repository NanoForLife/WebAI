# Course2023-2024

Resources and assignments for WebAI Course at KULeuven (2023-2024)

## How to Submit

* Fork the repository using the fork button on the page of the repository:
* Make any changes to your folder in the forked repository according to the specific assignment
* Commit and push your changes into your local forked repository
* Create a merge request (follow https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html under the section “When you work in a fork” 
to see how to create a merge request)
