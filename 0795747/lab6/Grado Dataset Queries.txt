Grado Dataset:
1) 
MATCH(n)	 return (n)

2) 
MATCH (p)-[:subClassOf]->(:Establishment) return p

3) 
MATCH (n)-[:type]-(:City) return n

4) 
MATCH (n:Santiago_de_Compostela) return n.hasInhabitantNumber

5)
MATCH (n:Santiago_de_Compostela), (o:Arzua) return n.label, n.hasInhabitantNumber, o.label, o.hasInhabitantNumber

6)
MATCH (n) WHERE n.hasInhabitantNumber IS NOT NULL RETURN distinct n.label, n.hasInhabitantNumber order by n.label desc

1)
MATCH (p)-[:type]-(n)-[:subClassOf]->(:Locality) where p.hasInhabitantNumber is not NULL return p.label, p.hasInhabitantNumber

2)
MATCH (n) WHERE n.hasInhabitantNumber > 200000 RETURN n.label, n.hasInhabitantNumber

3)
match (:Pazo_Breogan)-[:hasAddress]->(n) return n.hasStreet, n.hasNumber, n.label

4)
MATCH (p)-[:subClassOf]->(:Location) return p

5)
MATCH (p)-[:type]-()-[:subClassOf]-(:Locality) return p

6)
Match (n)-[:type]->(:City) RETURN count(n) > 0

