Query1:
MATCH()-[:hasOwningAirline]->(i) RETURN(count(distinct i))

Query2:
MATCH(n)-[:hasOwningAirline]->() RETURN(count(distinct n))
MATCH(n) WHERE n.hasStatus = "Active" RETURN(count(n))
MATCH(n) WHERE n.hasStatus = "Inactive" RETURN(count(n))

Query3:
MATCH(n {hasStatus:'Active'})-[:hasOwningAirline]->(i) RETURN i, count(n)

Query4:
MATCH(n)-[:type]->(:Aircraft) WHERE toInteger(substring(n.hasCreationDate, 0, 4)) < 2010 RETURN count(n)

Query5:
MATCH(n)-[:type]->(:Aircraft) WHERE n.hasModificationDate is not null RETURN count(n)
MATCH(n)-[:type]->(:Aircraft) WHERE n.hasModificationDate is not null and toInteger(substring(n.hasCreationDate, 0, 4)) + 1 = toInteger(substring(n.hasModificationDate, 0, 4)) RETURN n

Query6:
MATCH(n)-[:type]->(:AircraftModel) RETURN count(distinct n)

Query7:
MATCH(n)-[:hasModelVersion]->(i) WITH n, count(i) as num  WHERE num > 5 RETURN n, num

Query8:
MATCH(n)-[:type]->(:AircraftModel) RETURN distinct n.isManufacturedBy

Query9:
MATCH(n)-[:type]->(:AircraftModel) WHERE n.isManufacturedBy is null RETURN n

Query10:
MATCH (n)-[:type]->(c) UNWIND keys(n) as prop RETURN distinct c AS class, prop, n[prop] AS property_value