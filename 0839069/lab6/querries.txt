1)
MATCH (n)
RETURN n;

2)
MATCH (p)-[:subClassOf]->(:Establishment) 
RETURN p;

3)
MATCH (n)-[:type]->(:City)
RETURN n;

4)
MATCH (n:Santiago_de_Compostela) 
RETURN n.hasInhabitantNumber;

5)
MATCH (n)
WHERE n:Santiago_de_Compostela or n:Arzua
RETURN n.hasInhabitantNumber;

6)
MATCH (n)-[:type]->()-[:subClassOf]->(:Locality)
return n.hasInhabitantNumber as number, n as place
order by place

7)
MATCH (n)-[:type]->()-[:subClassOf]->(:Locality)
where n.hasInhabitantNumber is not null
return n.hasInhabitantNumber as number, n as place
order by place

8)
MATCH (n)-[:type]->()-[:subClassOf]-(:Locality)
where n.hasInhabitantNumber > 200000
return n.hasInhabitantNumber as number, n as place
order by place

9)
MATCH (:Pazo_Breogan)-[:hasAddress]->(addr)
Return  addr.hasStreet as street, addr.hasNumber as number, addr.label;

10)
Match (n)-[:subClassOf]->(:Location)
RETURN n

11)
Match (n)-[:type]->(class), (class)-[:subClassOf]->(:Locality)
RETURN n

12)
Match (n)-[:type]->(:Town)
RETURN count(n) > 0
