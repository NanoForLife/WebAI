1. Get all the classes
```
SELECT DISTINCT ?class WHERE {
 ?class rdf:type rdfs:Class.
}
```
2. Get all the subclasses of the class Establishment
```
SELECT DISTINCT ?subclass WHERE {
  ?subclass rdfs:subClassOf ex:Establishment
}
```
3. Get all instances of class City
```
SELECT ?instance WHERE{
  ?instance a ex:City.
}
```
4. Get the number of inhabitants of Santiago de Compostela
```
SELECT distinct ?inhab WHERE { 
    ex:Santiago_de_Compostela ex:hasInhabitantNumber ?inhab
}
```
5. Get the number of inhabitants of Santiago de Compostela and Arzua
```
SELECT DISTINCT ?place ?inhab WHERE { 
    ?place ex:hasInhabitantNumber ?inhab.
    FILTER (?place=ex:Arzua or ?place=ex:Santiago_de_Compostela).
}
```
6. Get different places with their number of inhabitants, ordered by the name of the place
```
SELECT DISTINCT ?place ?inhab WHERE { 
    ?place ex:hasInhabitantNumber ?inhab.
    ORDER BY asc(str(?s)).
}
```
7. Get all instances of the class Locality with their number of inhabitants (if it exists)
```
SELECT DISTINCT ?instance ?inhab WHERE { 
    ?instance rdf:type/rdfs:subClassOf ex:Locality. 
    OPTIONAL {?instance ex:hasInhabitantNumber ?inhab.}
}
```
8. Get all places with more than 200.000 inhabitants
```
SELECT DISTINCT ?place ?inhab WHERE { 
    ?place ex:hasInhabitantNumber ?inhab.
    FILTER (xsd:integer(?inhab) > 200000).
} 
```
9. Get all postal address details of Pazo_Breogan
```
SELECT ?address,?information WHERE { 
  ex:Pazo_Breogan ex:hasAddress ?address.
  ?address ?p ?information.
}
```
10.Get all subclasses of class Location
```
SELECT ?subclasses WHERE { 
  ?subclasses rdfs:subClassOf+ ex:Location.
}
```
11. Get all instances of class Locality
```
SELECT ?instance WHERE { 
  ?instance a ?class.
  ?class rdfs:subClassOf ex:Locality.
}
```
12. Describe the resource with rdfs:label "Madrid” 
```
DESCRIBE ?resource
WHERE {
  ?resource rdfs:label "Madrid"
}
```
13. Construct the RDF(S) graph that relates all touristic places with their provinces, using a new property ”isIn” 
```
CONSTRUCT {
 ?place ex:isIn ?province
}
WHERE {
 select ?place ?province
 WHERE{
  ?class rdfs:subClassOf ex:TouristicLocation.
  ?place a ?class.
  ?place ex:isPlacedIn ?city.
  ?city ex:inProvince ?province.
 }
}
```
14. Ask whether there is any instance of Town
```
ASK {
 ?s ?p ex:Town
}
```