# Grado Dataset Questions:

1. Get all the nodes
```
MATCH (n) RETURN n
```
2. Get all the subclasses of the class (node) Establishment
```
MATCH (subclasses)-[:subClassOf]->(:Establishment) RETURN nsubclasses
```
3. Get all instances of class (node) City
```
MATCH (instance)-[:type]->(:City) RETURN instance
```
4. Get the number of inhabitants of Santiago de Compostela
```
MATCH (n:Santiago_de_Compostela) RETURN (n.hasInhabitantNumber)
```
5. Get the number of inhabitants of Santiago de Compostela and Arzua
```
MATCH (s:Santiago_de_Compostela), (a:Arzua) RETURN s.hasInhabitantNumber,  a.hasInhabitantNumber
```
6. Get different places with their number of inhabitants, ordered by the name of the place in a descending order
```
MATCH (n) 
WHERE n.hasInhabitantNumber IS NOT NULL 
RETURN distinct n.label, n.hasInhabitantNumber order by n.label desc
```
7. Get all instances of the class (node) Locality with their number of inhabitants (if it exists)
```
MATCH (n)-[:type]-(p)-[:subClassOf]->(:Locality)
RETURN n.hasInhabitantNumber, n.label
```
8. Get all places with more than 200.000 inhabitants
```
MATCH (n)
WHERE n.hasInhabitantNumber > 200000 
RETURN n.label, n.hasInhabitantNumber 
```
9. Get all postal address details of Pazo_Breogan
```
MATCH (p)<-[:hasAddress]-(:Pazo_Breogan)
RETURN p.label, p.hasStreet, p.hasNumber
```
10. Get all subclasses of class (node) Location
```
MATCH (subclasses)-[:subClassOf*]->(:Location)
RETURN subclasses.label
```
11. Get all instances of class (node) Locality
```
MATCH (instance)-[:type]-()-[:subClassOf]->(:Locality)
RETURN instance.label
```
12. Ask whether there is any instance of Town
```
MATCH (instance)-[:type]-(:Town)
RETURN instance
```