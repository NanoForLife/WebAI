1. How many airlines inside?
```
MATCH(n:Airline)<-[:type]-() RETURN count(n)
```
2. How many aircrafts inside? how many are active? Inactive?
```
MATCH (:Aircraft)<-[:type]-(aircraft)
WHERE aircraft.hasStatus='Active'
RETURN aircraft.hasStatus,count(aircraft)
UNION ALL
MATCH (:Aircraft)<-[:type]-(aircraft)
WHERE aircraft.hasStatus='Inactive'
RETURN aircraft.hasStatus,count(aircraft)
```
3. Return the number of active aircrafts from each airline.
```
MATCH(aircraft)<-[:Owns]-(airline)-[:type]->(:Airline) 
WHERE aircraft.hasStatus="Active"
RETURN DISTINCT (airline), count(aircraft)
```
4. How many aircrafts were created before 2010?
```
MATCH (aircrafts)-[:type]->(:Aircraft)
WHERE toInteger(left(aircrafts.hasCreationDate, 4)) < 2010
RETURN count(aircrafts)
```
5. How many aircrafts were modified? how many were modified in the second year of its creation?
```
MATCH (aircrafts)-[:type]->(:Aircraft)
WHERE aircrafts.hasModificationDate is not null
RETURN count(aircrafts)
UNION ALL
MATCH (aircrafts)-[:type]->(:Aircraft)
WHERE toInteger(left(aircrafts.hasCreationDate, 4))+1 =  toInteger(left(aircrafts.hasModificationDate, 4))
RETURN count(aircrafts)
```
6. How many aircraft model existed in total?
```
MATCH (model)<-[:hasAircraftModel]-(aircraft)-[:type]-(:Aircraft)
RETURN count(distinct(model)) as AANTAL_MODELLEN
```
7. Return all the aircraft models that have more than 5 versions.
```
MATCH (n)<-[:hasModelVersion]-(model)-[:type]->(:AircraftModel)
WITH count(n) as count_models, model
WHERE count_models >= 5 
RETURN count_models, model.hasModelName
```
8. Return all the companies that manufacture these aircrafts.
```
MATCH (manu)-[:hasModelVersion]-()
WHERE manu.isManufacturedBy is not NULL
RETURN distinct(manu.isManufacturedBy) as manufacture_companies
```
9. How many aircraft models lack labels indicating their manufacturing companies?
```
MATCH (manu)-[:hasModelVersion]-()
WHERE manu.isManufacturedBy IS NULL 
RETURN COUNT(manu) as labels
```
10. Get all the properties for all the instances of all classes. (Property keys)
```
CALL db.propertyKeys()
YIELD propertyKey
RETURN DISTINCT propertyKey
```